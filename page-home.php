<?php
// Template Name: Página Home
get_header();
?>

<main>
    <section class="banner_home">
        <h1 id="banner_title">Comes&Bebes</h1>
        <h2 id="banner_description">O restaurante para todas as fomes</h2>
    </section>

    <section class="mostruario">
        <h2 class="midshow"> CONHEÇA NOSSA LOJA</h2>
        <div class="mostruario_cima">
            <h3>Tipos de pratos principais</h3>
            
            <?php
                wp_nav_menu(['menu' => 'categorias']);
            ?>
            
        </div>
        
            
        <div class="mostruario_baixo">
            <h3 id="dailyh3" >Pratos do dia de hoje: <br> <span class="weekDay"><?php echo $weekDay ?></span></h3>
            <div class="daily_foods">
                <?php
                    function renderProduct($weekDay) {
                        $args =[
                            'limit' => 4,
                            'orderby' => 'rand',
                            'order' => 'DESC',
                            'tag' => $weekDay
                        ];
                        $query = new WC_Product_Query($args);
                        $products = $query->get_products();
                            
                        return $products;
                    }
                    
                    $products = renderProduct($weekDay);

                    foreach ($products as $product) {  
                        formatar_produto_main($product);
                    }
                ?>
                
                
            </div>
            <div class="more_opt">
                <a class="show_more" href="http://comesbebes.local/loja/">Veja outras opções</a>
            </div>
        </div>
    </section>

    <section class="informacoes">
        <div class="info_cima">
            <h2 class="bellota" id="info_title">VISITE NOSSA LOJA FÍSICA</h2>
        </div>
        <div class="info_baixo">
            <div class="mapa">
                <div class="mapouter">
                    <div class="gmap_canvas">
                        <iframe src="https://maps.google.com/maps?q=<?php echo get_field('endereco_mapa') ?>&t=k&z=15&iwloc=&output=embed" width="345" height="205" style="border:0;" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                    
                </div>
                <p class="info_description"><?php echo get_field('endereco') ?></p>
                <p class="info_description"><?php echo get_field('telefone') ?></p>
            </div>
            <div>
                <img src="<?php echo IMAGES_DIR . '/pessoas.png' ?>" alt="pessoas">
            </div>
        </div>
    </section>
</main>

<?php
get_footer();
?>