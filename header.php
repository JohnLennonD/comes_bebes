<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?> | <?php the_title(); ?> </title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header class="header">
        <div class="header_left">
            <span class="logo_img">
                <?php the_custom_logo(); ?>
            </span>
            <div class="search">
                <img class="magnifier" src="<?php echo IMAGES_DIR . '/lupa.png' ?>" alt="icone lupa" >
                <input class="inp_text" type="text" placeholder="Pesquise aqui">
            </div>
        </div>

        <div class="header_right">
            <a class="btn_order" href="/">Faça um pedido</a>
            <a href="http://comesbebes.local/carrinho/"><img src="<?php echo IMAGES_DIR . '/carrinho_image.png' ?>" alt="carrinho"></a>
            <a href="/painel"><img src="<?php echo IMAGES_DIR . '/icone_perfil.png' ?>" alt="icone"></a>
        </div>
        
    </header>