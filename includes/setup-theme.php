<?php
function cb_setup_theme() {
    add_theme_support('custom-logo');
    // add_theme_support('menus');
    add_theme_support('woocommerce');

    register_nav_menus([
        'esquerdo' => 'Menu Lateral Esquerdo',
        'direito' => 'Menu Lateral Direito'
    ]);
}

// função para setar a quantidade de lobos mostrada na página
function cb_products_per_page() {
    return 8;
}

?>