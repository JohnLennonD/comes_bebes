<?php

function cb_enqueue_style(){
    wp_register_style('style', get_stylesheet_uri(), [], '1.0.0', false);
    wp_register_style('cb-header', STYLES_DIR . '/header.css' , [], '1.0.0', false);
    wp_register_style('cb-main', STYLES_DIR . '/main.css' , [], '1.0.0', false);
    wp_register_style('cb-footer', STYLES_DIR . '/footer.css' , [], '1.0.0', false);
    wp_register_style('cb-painel', STYLES_DIR . '/painel.css' , [], '1.0.0', false);
    wp_register_style('cb-products', STYLES_DIR . '/products.css' , [], '1.0.0', false);
    wp_register_style('cb-shop-product', STYLES_DIR . '/shop_product.css' , [], '1.0.0', false);


    wp_enqueue_style('style');
    wp_enqueue_style('cb-header');
    wp_enqueue_style('cb-main');
    wp_enqueue_style('cb-footer');
    wp_enqueue_style('cb-painel');
    wp_enqueue_style('cb-products');
    wp_enqueue_style('cb-shop-product');
}

?>