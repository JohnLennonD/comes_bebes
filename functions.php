<?php

// Variáveis
define('ROOT_DIR', get_theme_file_path());
define('STYLES_DIR', get_template_directory_uri() . '/assets/css');
define('IMAGES_DIR', get_template_directory_uri() . '/assets/images');
define('INCLUDES_DIR', ROOT_DIR . '/includes');

// Includes
include_once(INCLUDES_DIR . '/enqueue.php');
include_once(INCLUDES_DIR . '/setup-theme.php');
include_once(INCLUDES_DIR . '/format_product.php');

// Ganchos
add_action('wp_enqueue_scripts', 'cb_enqueue_style');
add_action('after_setup_theme', 'cb_setup_theme');
add_filter('loop_shop_per_page', 'cb_products_per_page');

function transforma_menu($itens){
  foreach($itens as $item){
    $thumbnail_id = get_term_meta($item->object_id, 'thumbnail_id', true);
    $image = wp_get_attachment_url($thumbnail_id);
    $css = 'background-image: url('. $image .')';
    $item->title = 
    '<div class="Categoria-content" style="'. $css .';">
      <div class="Categoria-titulo">
        <p class="Title">'.$item->title.'</p>
      </div>
    </div>';
  }
  return $itens;
}

  
add_filter('wp_nav_menu_objects', 'transforma_menu');
// add_filter('wp_nav_menu_objects', 'transforma_menu_food');


function getDayOfWeek() {
  $dayOfWeek = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');

  $data = date('Y-m-d');

  $dayOfWeek_num = date('w', strtotime($data));

  return $dayOfWeek[$dayOfWeek_num];
}

$weekDay = getDayOfWeek();


?>