<?php

function formatar_produto_main($typefood)
{
    $imageid = $typefood->get_image_id();
    $imageurl = wp_get_attachment_url($imageid);
    $css = 'background-image: url(' . $imageurl . ')';
    $slug = $typefood->get_slug();
    $linkproduct = 'http://comesbebes.local/product/' . $slug;
?>
    <div class="card-typefood" style=" <?= $css ?>">
        <div class="product-content">
            <div class="content_info">
                <p><?= $typefood->get_name(); ?></p>
                <div class="botton_content">
                    <p><?= $typefood->get_price_html() ?></p>
                    <a href="<?= $typefood->get_permalink(); ?>"><img src="<?php echo IMAGES_DIR . '/carrinho_plus.png' ?>" alt="adicionar <?= $slug; ?> ao carrinho"></a>
                </div>
            </div>
        </div>
    </div>
<?php
    return $linkproduct;
}

?>