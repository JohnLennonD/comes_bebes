<?php
    get_header();
?>

<main id="painel">

    <section class="navigation_bar">
        <div>
            <ul class="navigation_menus">
                <li><a href="/painel">PAINEL</a></li>
                <li><a href="/pedidos">PEDIDOS</a></li>
                <li><a href="/enderecos">ENDEREÇOS</a></li>
                <li><a href="/home">SAIR</a></li>
            </ul>
        </div>
    </section>

    <section class="form_out">

        <p>
            Olá, <!--?php echo user(); ?--> User (não é User? Sair)
            <br>
            A partir do painel de controle de sua conta, você pode ver suas compras recentes, gerenciar seus endereços de entrega e faturamento, e editar sua senha e detalhes da conta.
        </p><br>

        <div class="form_in">
            
            <div id="nome_sobrenome_out">

                <div class="inp_div" id="nome_sobrenome_in">
                <label for="nome">Nome</label>
                <input class="inp_painel" type="text" name="nome" id="nome" placeholder="Digite seu nome">
                </div>
                
                <div class="inp_div" id="nome_sobrenome_in">
                <label for="sobrenome">Sobrenome</label>
                <input class="inp_painel" type="text" name="sobrenome" id="sobrenome" placeholder="Digite seu sobrenome">
                </div>

            </div>
            
            <div class="inp_div" id="form_out">
                <label for="cep">CEP</label>
                <input class="inp_painel" type="text" name="cep" id="cep" placeholder="Digite seu CEP">
            </div>
            
            <div class="inp_div" id="form_out">
                <label for="logradouro">Logradouro</label>
                <input class="inp_painel" type="text" name="logradouro" id="logradouro" placeholder="Rua e número">
            </div>

            <div class="inp_div" id="form_out">
                <label for="complemento">Complemento</label>
                <input class="inp_painel" type="text" name="complemento" id="complemento" placeholder="Complemento do seu endereço">
            </div>

            <div id="nome_sobrenome_out">

                <div class="inp_div" id="nome_sobrenome_in">
                <label for="bairro">Bairro</label>
                <input class="inp_painel" type="text" name="bairro" id="bairro" placeholder="Seu bairro">
                </div>
                
                <div class="inp_div" id="nome_sobrenome_in">
                <label for="cidade">Cidade</label>
                <input class="inp_painel" type="text" name="cidade" id="cidade" placeholder="Sua cidade">
                </div>

            </div>
            
        </div>
            
    </section>
    
    <div id="div_btn_painel">
        <button id="btn_painel" type="submit">SALVAR ALTERAÇÕES</button>
    </div>

</main>

<?php
    get_footer();
?>