<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

get_header();

?>

<section class="section_shop">
	<div class="show_section">
		<div class="menu_top">
			<h2 class="title">SELECIONE UMA CATEGORIA</h2>
			<?php wp_nav_menu(['menu' => 'categorias']); ?>
		</div>
		<div class="search_food">
			<h2>PRATOS</h2>
			<div class="basket">
				<span class="categ">COMIDA</span>
				<label for="nome">Bucar por nome:</label>
				<input type="text" id="nome">
			</div>
			<div class="filter">
				<div class="ordering">
					<label for="">Ordenar por:</label>
					<?php woocommerce_catalog_ordering(); ?>
				</div>
				<div class="filter_price">
					<label for="">Filtro de preço:</label>
					<span>
						De: <input type="text" name="food-price-ini">
						Até: <input type="text" name="food-price-end">
					</span>
				</div>

			</div>
		</div>
		<div class="list_products">
			<?php
 
			if(have_posts()){
				while(have_posts()){
					the_post();
					$id = get_the_ID();
					formatar_produto_main(wc_get_product($id));
				}
			}
			
			?>
		</div>
		<div class="pagination">
			<?php echo get_the_posts_pagination(array(
				'prev_text' => '<',
				'next_text' => '>'
			)); ?>
		</div>
	</div>
</section>

<?php

get_footer();

?>