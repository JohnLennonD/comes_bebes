<?php
    get_header();
?>

<main id="painel">

    <section class="navigation_bar">
        <div>
            <ul class="navigation_menus">
                <li><a href="/painel">PAINEL</a></li>
                <li><a href="/pedidos">PEDIDOS</a></li>
                <li><a href="/enderecos">ENDEREÇOS</a></li>
                <li><a href="/home">SAIR</a></li>
            </ul>
        </div>
    </section>

    <section class="form_out">

        <p>
            Olá, <!--?php echo user(); ?--> User (não é User? Sair)
            <br>
            A partir do painel de controle de sua conta, você pode ver suas compras recentes, gerenciar seus endereços de entrega e faturamento, e editar sua senha e detalhes da conta.
        </p><br>

        <div class="form_in">
            
            <div id="nome_sobrenome_out">

                <div class="inp_div" id="nome_sobrenome_in">
                <label for="nome">Nome</label>
                <input class="inp_painel" type="text" name="nome" id="nome" placeholder="Digite seu nome">
                </div>
                
                <div class="inp_div" id="nome_sobrenome_in">
                <label for="sobrenome">Sobrenome</label>
                <input class="inp_painel" type="text" name="sobrenome" id="sobrenome" placeholder="Digite seu sobrenome">
                </div>

            </div>
            
            <div class="inp_div" id="form_out">
                <label for="email">Email</label>
                <input class="inp_painel" type="text" name="email" id="email" placeholder="Digite seu email">
            </div>
            
            <div class="inp_div" id="form_out">
                <label for="senha">Senha atual (deixe em branco para não alterar)</label>
                <input class="inp_painel" type="text" name="senha" id="senha" placeholder="Digite sua senha atual ">
            </div>

            <div class="inp_div" id="form_out">
                <label for="novasenha">Nova senha (deixe em branco para não alterar)</label>
                <input class="inp_painel" type="text" name="novasenha" id="novasenha" placeholder="Digite sua nova senha">
            </div>

            <div class="inp_div" id="form_out">
                <label for="confsenha">Confirmar nova senha</label>
                <input class="inp_painel" type="text" name="confsenha" id="confsenha" placeholder="Confirme sua nova senha ">
            </div>
            
        </div>
            
    </section>
    
    <div id="div_btn_painel">
        <button id="btn_painel" type="submit">SALVAR ALTERAÇÕES</button>
    </div>

</main>

<?php
    get_footer();
?>